package ts;

import java.awt.*;
import java.awt.event.*;

import java.io.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class RWindow extends JComponent {
	private int WIDTH = 48, HEIGHT = 36;
	private JPanel inputPanel;
	private JTextArea inputDisplay;
	private JScrollPane inputScroll;
	private JFrame frame;
	private RControl control = new RControl(this);
	private Font font = new Font("Helvetica", Font.BOLD, 14);
	private Color gColor = Color.BLACK;
	
	RWindow() {
		this.setOpaque(true);
		this.addMouseListener(new MouseHandler());
		this.addMouseMotionListener(new MouseMotionHandler());
		
		inputPanel = new JPanel();
		inputPanel.setBorder(new TitledBorder(new EtchedBorder(), "Input"));
		inputDisplay = new JTextArea(HEIGHT, WIDTH);
		inputDisplay.addKeyListener(new KeyHandler());
		inputDisplay.setEditable(true);
		inputDisplay.setFont(font);
		inputDisplay.setForeground(gColor);
		inputDisplay.setLineWrap(true);
		inputDisplay.setWrapStyleWord(true);
		inputScroll = new JScrollPane(inputDisplay);
		inputScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		inputPanel.add(inputScroll);

		frame = new JFrame("Reflection & Prayer");
		control.setFloatable(false);
		frame.add(control, BorderLayout.NORTH);
		frame.getRootPane().setDefaultButton(control.getDefaultButton());
		SpringLayout layout = new SpringLayout();
		frame.setLayout(layout);
		frame.add(inputPanel);
		SpringUtilities.makeCompactGrid(frame.getContentPane(), 2, 1, 5, 5, 5, 5);
		frame.pack();
		frame.addWindowListener(new WindowHandler());
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	void newPage() {
		inputDisplay.setText("");
		inputDisplay.setForeground(gColor);
	}

	void newPractice() {
		inputDisplay.setText("");
	}

	void save() {
		String name = JOptionPane.showInputDialog("Please enter file name.", JOptionPane.QUESTION_MESSAGE);
		if (name != null)
			Utils.write("reflection", "reflection/" + name, inputDisplay.getText(), true);
	}

	void open() {
		try {
			JPanel p = new JPanel();
			//p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
			p.add(new JLabel("Select file :"));
			//p.add(new JTextField(10));
			File dir = new File("reflection");
			String[] myFiles = null;
			if (dir.exists()) {
				myFiles = dir.list(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.charAt(0) != 46;
					}
				});
			}
			JComboBox<String> box;
			if (myFiles != null)
				box = new JComboBox<String>(myFiles);
			else
				box = new JComboBox<String>();
			p.add(box);
			int option = JOptionPane.showConfirmDialog(null, p, "Open an existing reference file",
	 									 JOptionPane.OK_CANCEL_OPTION);
			if (option == JOptionPane.OK_OPTION) {
				String name = (String) box.getSelectedItem();
				String content = Utils.read("reflection/" + name);
				if (content != null) {
					inputDisplay.setForeground(gColor);
				}
			}
		} catch (Exception e) {
			Utils.error("error", "Cannot open source folder");
		}
	}

	void imp() {
		String name = JOptionPane.showInputDialog("Please enter file path.", JOptionPane.QUESTION_MESSAGE);
		String content = null;
		if (name != null)
			content = Utils.read(name);
		if (content != null) {
			inputDisplay.setForeground(gColor);
		}
	}

	void exp() {

	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}

	@Override
	public void paintComponent(Graphics g) {

	}

	private class WindowHandler extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			//System.exit(0);
			RWindow.this.frame.setVisible(false);
		}
	}

	private class MouseMotionHandler extends MouseMotionAdapter {

		@Override
	    public void mouseDragged(MouseEvent e) {
	        //Window.this.resize = true;
	    }
	}	

	private class MouseHandler extends MouseAdapter {

		@Override
		public void mouseReleased(MouseEvent e) {
			//Window.this.resize = false;
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			//Window.this.resize = false;
			
		}

	}

	private class KeyHandler extends KeyAdapter {

		@Override
		public void keyTyped(KeyEvent e) {

		}

		@Override
		public void keyPressed(KeyEvent e) {
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			
		}

	}
}