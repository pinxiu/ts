package ts;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

public class Utils {
	public static void error(String type, String message, String... args) {
		switch (type) {
			case "Invalid":
				JOptionPane.showMessageDialog(null, message, "Failure", JOptionPane.ERROR_MESSAGE);
				break;
			case "error":
				JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
				break;
			case "help":
				JOptionPane.showMessageDialog(null, message, "Instruction", JOptionPane.INFORMATION_MESSAGE);
				break;
			default: // Do nothing.
		}
		
	}

	public static void write(String dirName, String name, String content, boolean save) {
		try {
			if (save) {
				File dir = new File(dirName);
				if (!dir.exists())
					dir.mkdir();
			}
            File statText = new File(name);
            while (statText.exists()) {
            	Object[] options = {"Yes, choose a different file name.", "No, rewrite the file."};
				int choice = JOptionPane.showOptionDialog(null, "A file with the same name already exists. Do you want to choose a different file name?",
					"Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
				if (choice == JOptionPane.NO_OPTION)
					break;
				else {
					name = JOptionPane.showInputDialog("Please enter file name.");
					if (name == null)
						break;
					else
						statText = new File(name);
				}
            }
            FileOutputStream is = new FileOutputStream(statText);
            OutputStreamWriter osw = new OutputStreamWriter(is);    
            Writer w = new BufferedWriter(osw);
            w.write(content);
            w.close();
        } catch (IOException e) {
            error("Invalid", "Error writing to the file '" + name + "' !");
        }
	}

	public static String read(String fileName) {
		try {
            FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line = null;
			String content = "";

            while((line = bufferedReader.readLine()) != null) {
                content += line;
                content += "\n";
            }   
            bufferedReader.close();
            return content;        
        }
        catch (FileNotFoundException ex) {
            error("Invalid", "Cannot find file \"" + fileName + "\" !");
            return null;             
        }
        catch (IOException ex) {
            error("Invalid", "Error reading file \"" + fileName + "\" !");
            return null;             
        }
	}
}