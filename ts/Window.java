package ts;

import java.awt.*;
import java.awt.event.*;

import java.io.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class Window extends JComponent {
	private int WIDTH = 48, HEIGHT = 36;
	private JPanel inputPanel, refPanel;
	private JTextArea inputDisplay, refDisplay;
	private JScrollPane inputScroll, refScroll;
	private JFrame frame;
	private boolean practice = false;
	private boolean hasText = false;
	private boolean hide = false;
	private Control control = new Control(this);
	private String input, ref;
	private Font font = new Font("Helvetica", Font.BOLD, 14);
	private Color gColor = Color.BLACK;
	private Color pColor = Color.BLUE;
	private Color bColor = Color.RED;
	private Color rColor = Color.lightGray;
	private boolean correct = false;

	Window() {
		this.setOpaque(true);
		this.addMouseListener(new MouseHandler());
		this.addMouseMotionListener(new MouseMotionHandler());
		
		inputPanel = new JPanel();
		inputPanel.setBorder(new TitledBorder(new EtchedBorder(), "Input"));
		inputDisplay = new JTextArea(HEIGHT/2, WIDTH);
		inputDisplay.addKeyListener(new KeyHandler());
		inputDisplay.setEditable(true);
		inputDisplay.setFont(font);
		inputDisplay.setForeground(gColor);
		inputDisplay.setLineWrap(true);
		inputDisplay.setWrapStyleWord(true);
		inputScroll = new JScrollPane(inputDisplay);
		inputScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		inputPanel.add(inputScroll);

		refPanel = new JPanel();
		refPanel.setBorder(new TitledBorder(new EtchedBorder(), "Reference"));
		refDisplay = new JTextArea(HEIGHT/2, WIDTH);
		refDisplay.setEditable(false);
		refDisplay.setFont(font);
		refDisplay.setForeground(gColor);
		refDisplay.setLineWrap(true);
		refDisplay.setWrapStyleWord(true);
		refScroll = new JScrollPane(refDisplay);
		refScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		refPanel.add(refScroll);

		frame = new JFrame("Verses Memorization");
		control.setFloatable(false);
		frame.add(control, BorderLayout.NORTH);
		frame.getRootPane().setDefaultButton(control.getDefaultButton());
		SpringLayout layout = new SpringLayout();
		frame.setLayout(layout);
		frame.add(inputPanel);
		frame.add(refPanel);
		SpringUtilities.makeCompactGrid(frame.getContentPane(), 3, 1, 5, 5, 5, 5);
		frame.pack();
		frame.addWindowListener(new WindowHandler());
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	void newPage() {
		inputDisplay.setText("");
		this.practice = false;
		this.correct = true;
		if (hide) {
			this.hasText = false;
			this.ref = "";
			hide = false;
		}
		inputDisplay.setForeground(gColor);
		refDisplay.setForeground(gColor);
	}

	void newPractice() {
		inputDisplay.setText("");
	}

	void save() {
		String name = JOptionPane.showInputDialog("Please enter file name.", JOptionPane.QUESTION_MESSAGE);
		if (name != null)
			Utils.write("verses", "verses/" + name, inputDisplay.getText(), true);
	}

	void open() {
		try {
			JPanel p = new JPanel();
			p.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.HORIZONTAL;
			//p.add(new JTextField(10));
			File dir = new File("verses");
			String[] myFiles = null;
			if (dir.exists()) {
				myFiles = dir.list(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.charAt(0) != 46;
					}
				});
			}
			JComboBox<String> box;
			if (myFiles != null)
				box = new JComboBox<String>(myFiles);
			else
				box = new JComboBox<String>();
			JComboBox<String> box2 = new JComboBox<String> (new DefaultComboBoxModel<String>());
			box.addActionListener (new ActionListener () {
			    public void actionPerformed(ActionEvent e) {
			    	String name = (String) box.getSelectedItem();
					File book = new File("verses/" + name);
					String[] chapters = null;
					if (book.exists()) {
						chapters = book.list(new FilenameFilter() {
							public boolean accept(File dir, String name) {
								return name.charAt(0) != 46;
							}
						});
					}
			        box2.setModel(new DefaultComboBoxModel<String>(chapters));    
			    }
			});
			gbc.gridx = 0;
			gbc.gridy = 0;
			//gbc.weightx = 0.0;
			p.add(new JLabel("Select book :"), gbc);
			gbc.gridx = 1;
			gbc.gridy = 0;
			//gbc.weightx = 4;
			box.setPrototypeDisplayValue("XXXXXXXXXXXXXXXX");
			p.add(box, gbc);
			gbc.gridx = 0;
			gbc.gridy = 1;
			//gbc.weightx = 0.5;
			p.add(new JLabel("Select passage :"), gbc);
			gbc.gridx = 1;
			gbc.gridy = 1;
			//gbc.weightx = 4;
			//box2.setMaximumSize(box2.getPreferredSize());
			box2.setPrototypeDisplayValue("XXXXXXXXXXXXXXXX");
			p.add(box2, gbc);
			
			int option = JOptionPane.showConfirmDialog(null, p, "Open an existing reference file",
	 									 JOptionPane.OK_CANCEL_OPTION);
			if (option == JOptionPane.OK_OPTION) {
				String name = (String) box.getSelectedItem();
				String doc = (String) box2.getSelectedItem();
				String content = Utils.read("verses/" + name + "/" + doc);
				if (content != null) {
					inputDisplay.setForeground(gColor);
					refDisplay.setForeground(gColor);
					refDisplay.setText(content);
					hasText = true;
				}
			}
		} catch (Exception e) {
			Utils.error("error", "Cannot open source folder");
		}
	}

	void imp() {
		String name = JOptionPane.showInputDialog("Please enter file path.", JOptionPane.QUESTION_MESSAGE);
		String content = null;
		if (name != null)
			content = Utils.read(name);
		if (content != null) {
			inputDisplay.setForeground(gColor);
			refDisplay.setForeground(gColor);
			refDisplay.setText(content);
			hasText = true;
		}
	}

	boolean hasText() {
		return hasText;
	}

	void setPractice() {
		if (!practice) {
			this.practice = true;
			this.ref = refDisplay.getText();
		}
		this.correct = true;
		inputDisplay.setForeground(pColor);
		refDisplay.setForeground(rColor);
	}

	boolean isPractice() {
		return practice;
	}

	void setHide() {
		this.hide = true;
		refDisplay.setText("");
	}

	boolean isHide() {
		return hide;
	}

	void setShow() {
		this.hide = false;
		refDisplay.setText(ref);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}

	@Override
	public void paintComponent(Graphics g) {

	}

	private class WindowHandler extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			//System.exit(0);
			Window.this.frame.setVisible(false);
		}
	}

	private class MouseMotionHandler extends MouseMotionAdapter {

		@Override
	    public void mouseDragged(MouseEvent e) {
	        //Window.this.resize = true;
	    }
	}	

	private class MouseHandler extends MouseAdapter {

		@Override
		public void mouseReleased(MouseEvent e) {
			//Window.this.resize = false;
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			//Window.this.resize = false;
			
		}

	}

	private class KeyHandler extends KeyAdapter {

		@Override
		public void keyTyped(KeyEvent e) {
			if (Window.this.isPractice()) {
	    		for (int i = 0; i < Window.this.ref.length(); i++) {
		    		if (i == Window.this.inputDisplay.getText().length()) {
		    			Window.this.correct = true;
		    			break;
		    		}
		    		if (Window.this.ref.charAt(i) != Window.this.inputDisplay.getText().charAt(i)) {
		    			Window.this.correct = false;
		    			break;
		    		}
		    	}
		    	if (Window.this.correct == true)
		    		Window.this.inputDisplay.setForeground(pColor);
		    	else
		    		Window.this.inputDisplay.setForeground(bColor);
		    }
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (Window.this.isPractice()) {
				if (Window.this.inputDisplay.getText().length() > Window.this.ref.length()) {
					Window.this.correct = false;
				} else {
		    		for (int i = 0; i < Window.this.ref.length(); i++) {
			    		if (i == Window.this.inputDisplay.getText().length()) {
			    			Window.this.correct = true;
			    			break;
			    		}
			    		if (Window.this.ref.charAt(i) != Window.this.inputDisplay.getText().charAt(i)) {
			    			Window.this.correct = false;
			    			break;
			    		}
			    	}
			    }
		    	if (Window.this.correct == true)
		    		Window.this.inputDisplay.setForeground(pColor);
		    	else
		    		Window.this.inputDisplay.setForeground(bColor);
		    }
		}

		@Override
		public void keyReleased(KeyEvent e) {
			if (Window.this.isPractice()) {
	    		for (int i = 0; i < Window.this.ref.length(); i++) {
		    		if (i == Window.this.inputDisplay.getText().length()) {
		    			Window.this.correct = true;
		    			break;
		    		}
		    		if (Window.this.ref.charAt(i) != Window.this.inputDisplay.getText().charAt(i)) {
		    			Window.this.correct = false;
		    			break;
		    		}
		    	}
		    	if (Window.this.correct == true)
		    		Window.this.inputDisplay.setForeground(pColor);
		    	else
		    		Window.this.inputDisplay.setForeground(bColor);
		    }
		}

	}
}