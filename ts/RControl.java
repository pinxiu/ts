package ts;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class RControl extends JToolBar {
	private Action newPage = new NewPageAction("New");
	private Action open = new OpenAction("Open");
	private Action save = new SaveAction("Save");
	private Action imp = new ImportAction("Import");
	private Action exp = new ExportAction("Export");
	private Action help = new HelpAction("Help");
	
	private JButton defaultButton = new JButton(newPage);
	private RWindow window;

	JButton getDefaultButton() {
		return defaultButton;
	}

	RControl(RWindow window) {
		this.window = window;
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.setOpaque(true);

		this.add(defaultButton);
		this.add(new JButton(open));
		this.add(new JButton(save));
		this.add(new JButton(imp));
		this.add(new JButton(exp));
		this.add(new JButton(help));
		
	}

	private class NewPageAction extends AbstractAction {
		public NewPageAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			window.newPage();
		}
	}

	private class SaveAction extends AbstractAction {
		public SaveAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			window.save();
		}
	}

	private class OpenAction extends AbstractAction {
		public OpenAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			window.open();
		}
	}

	private class ImportAction extends AbstractAction {
		public ImportAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			window.imp();
		}
	}

	private class ExportAction extends AbstractAction {
		public ExportAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			window.exp();
		}
	}

	private class HelpAction extends AbstractAction {
		public HelpAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			String text = "New:\n";
			text = text + "         Start a new page in \"Input\" area. If the content in\n";
			text = text + "         \"Reference\" area is hidden, it is erased.\n";
			text = text + "Save:\n";
			text = text + "         Save the current content in \"Input\" area.\n";
			text = text + "Open:\n";
			text = text + "         Show the content of target file in \"Reference\" area.\n";
			text = text + "Practice:\n";
			text = text + "         The content in \"Input\" area turns red if it doesn't\n";
			text = text + "         match the content in \"Reference\" area.\n";
			text = text + "Hide:\n";
			text = text + "         Hide the content in \"Reference\" area.\n";
			text = text + "Show:\n";
			text = text + "         Show the content in \"Reference\" area.";
			Utils.error("help", text);
		}
	}

}