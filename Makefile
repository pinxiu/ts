.PHONY: ts clean launch build move

ts: 
	javac ts/*.java
	java ts/ThirdSpace
	
clean:
	rm -f ts/*.class
	
launch:
	jar cmvf MainClass.txt ts.jar ts/*.class files

build:
	mkdir -p package/macosx
	cp ThirdSpace.icns package/macosx
	
move:
	cp deploy/bundles/ThirdSpace-1.0.dmg third-space-installer.dmg
