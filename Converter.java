import java.io.*;

public class Converter {

	public static int sell(String cake) {
		File odir = new File(cake);
		try {
			if (!odir.exists())
				odir.mkdir();
			return 1;
		} catch (Exception e) {
			return 0;
		}
	}

	public static int bake(String flour, String cake) {
		File indir = new File(flour);
		if (!indir.exists())
			return 0;

		return read(indir, cake);

	}

	public static int writer(String name, String content, String cake) {
		try {
			File statText = new File(cake + "/" + name);
            
            FileOutputStream is = new FileOutputStream(statText, true);
            OutputStreamWriter osw = new OutputStreamWriter(is);    
            Writer w = new BufferedWriter(osw);
            w.write(content);
            w.close();
            return 1;
        } catch (IOException e) {
            System.out.println("Error writing to the file '" + name + "' !");
            return 0;
        }
	}

	private static int getInt(int prefix, String line) {
		if (line.length() == 0)
			return prefix;
		else if (line.charAt(0) == 32) {
			//System.out.println((int) line.charAt(0));
			return 0;
		} else if (line.charAt(0) < 48 || line.charAt(0) > 57) {
			//System.out.println((int) line.charAt(0));
			return prefix;
		} else {
			//System.out.println((int) line.charAt(0));
			return getInt(prefix * 10 + line.charAt(0) - 48, line.substring(1));
		}
	}

	private static String cook(int len, int num, String content) {
		int first = content.indexOf(String.valueOf(num));
		if (first == -1 || (content.length() > len && content.charAt(first + len) == 32))
			return content;
		else {
			return content.substring(0, first) + cook(len, num, content.substring(first + len));
		}
	}

	public static int write(String name, String content, String cake) {
		int count = 1;
		String title = name + " " + Integer.toString(count);

		String[] lines = content.split("\n");

		for (String line : lines) {
			line = line.trim();
			line += "\n";
			int head = getInt(0, line);
			//System.out.println(title);
			//System.out.println(head);
			//System.out.println(line);
			if (head == count + 1) {
				count++;
				title = name + " " + Integer.toString(count);
			}
			int err = writer(title, cook(String.valueOf(count).length(), count, line), cake);
			if (err == 0) {
				return 0;
			}
		}
		return 1;
	}

	public static int read(File fileName, String cake) {
		try {
            FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line = null;
			String content = "";
			String name = null;
			int err = 1;

            while((line = bufferedReader.readLine()) != null) {
            	if (line.equals("/ * End of Book * /")) {
            		err = write(name, content, cake);
            		if (err == 0)
            			return 0;
            		break;
            	}
            	if (line.equals("/ * Start of Book * /")) {
            		if (name != null) {
            			try {
            				File odir = new File(cake + "/" + name);
							if (!odir.exists())
								odir.mkdir();
						} catch (Exception e) {
							return 0;
						}
            			err = write(name, content, cake + "/" + name);
            		}
            		if (err == 0)
            			return 0;
            		content = "";
            		name = bufferedReader.readLine();
            	} else {
	                content += line;
	                content += "\n";
	            }
            }   
            bufferedReader.close();
            return 1;        
        }
        catch (FileNotFoundException ex) {
            System.out.println("Cannot find file \"" + fileName + "\" !");
            return 0;             
        }
        catch (IOException ex) {
            System.out.println("Error reading file \"" + fileName + "\" !");
            return 0;             
        }
	}

	public static void main(String[] args) {
		int err = sell(args[1]) & bake(args[0], args[1]);
		if (err == 1) {
			System.out.println("Ready!");
		} else {
			System.out.println("Failed!");
		}
	}

}